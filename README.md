# ParceroFunk build for the _st_ terminal

## Patches to date
- [alpha](https://st.suckless.org/patches/alpha/): to give the terminal a transparent background.
- [nordtheme](https://st.suckless.org/patches/nordtheme/): Nord color scheme as personal preference.
- [scrollback](https://st.suckless.org/patches/scrollback/): to give the terminal scrollback functionality.
- [font2](https://st.suckless.org/patches/font2/): extra font for fallback situation.
